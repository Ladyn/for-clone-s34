## Usage Instructions

### NPM Install
1. Make sure to issue npm install inside both the api and client folders

### .env files
1. In the api folder, create a file named .env

In api/.env, add the following:

MAILING_SERVICE_CLIENT_ID=""

MAILING_SERVICE_CLIENT_SECRET=""

MAILING_SERVICE_REFRESH_TOKEN=""

MAILING_SERVICE_EMAIL=""

In each entry, make sure to add the appropriate credentials from our email sending lesson inside the quotation marks.

2. In the client folder, create a file named .env.local

In client/.env.local, add the following:

NEXT_PUBLIC_API_URL=http://localhost:4000/api

### MongoDB Connection

1. In api/index.js, change the contents of mongoose.connect(...) (line 10) so that your MongoDB connection details are inside of the quotation marks.

### Client IDs

1. In api/controllers/user.js, change the value of const clientId (line 6) to match the client ID in your .env file.

2. In client/pages/login/index.js, change the value of the clientId attribute (line 107) for the GoogleLogin component to match the client ID in your .env file.

### Email sending

1. In api/controllers/user.js, change the value of "to" (line 88) to an email address you'd like to receive email confirmation from.